/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.Id;

/**
 *
 * @author GUSTAVO
 */
@Entity
public class Prova implements Serializable {

    @Id
    private int ID;
    private String Descricao;

    public Prova(int ID, String Descricao) {
        this.ID = ID;
        this.Descricao = Descricao;
    }
    
    public Prova() {
        this.ID = 0;
        this.Descricao = "";
    }
    
    public long getID() {
        return ID;
    }

    public void setID(int ID) {
        this.ID = ID;
    }

    public String getDescricao() {
        return Descricao;
    }

    public void setDescricao(String Descricao) {
        this.Descricao = Descricao;
    }

    @Override
    public String toString() {
        return "Prova:" + "ID=" + this.getID() + ", Descricao=" + this.getDescricao();
    }
    
    
    
}
