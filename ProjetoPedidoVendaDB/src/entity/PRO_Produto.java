
package entity;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class PRO_Produto implements Serializable {

    @Id
    private String PRO_ProdutoCodigo;
    private String PRO_ProdutoDescricao;
    
    public PRO_Produto (){
        this.PRO_ProdutoCodigo = "";
        this.PRO_ProdutoDescricao = "";
        
    }

    @Override
    public String toString() {
        return "Produto:" + this.getPRO_ProdutoCodigo() + " - " + this.getPRO_ProdutoDescricao();
    }

    public PRO_Produto(String PRO_ProdutoCodigo, String PRO_ProdutoDescricao) {
        this.PRO_ProdutoCodigo = PRO_ProdutoCodigo;
        this.PRO_ProdutoDescricao = PRO_ProdutoDescricao;
    }

    public String getPRO_ProdutoCodigo() {
        return PRO_ProdutoCodigo;
    }

    public void setPRO_ProdutoCodigo(String PRO_ProdutoCodigo) {
        this.PRO_ProdutoCodigo = PRO_ProdutoCodigo;
    }

    public String getPRO_ProdutoDescricao() {
        return PRO_ProdutoDescricao;
    }

    public void setPRO_ProdutoDescricao(String PRO_ProdutoDescricao) {
        this.PRO_ProdutoDescricao = PRO_ProdutoDescricao;
    }
    
}
