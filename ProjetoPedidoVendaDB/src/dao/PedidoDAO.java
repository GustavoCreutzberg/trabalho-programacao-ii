/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import entity.PDV_Pedido;
import java.util.List;
import javax.persistence.Query;

/**
 *
 * @author GUSTAVO
 */
public class PedidoDAO extends DAO {
    private static final long serialVersionUID = 1L;

    @SuppressWarnings("unchecked")
    public static List<PDV_Pedido> listar() {
        Query q = criarQuery("SELECT p FROM PDV_Pedido p ORDER BY p.PDV_PedidoSeq");
        List<PDV_Pedido> l = q.getResultList();
        System.out.println("total: "+ l.size());
        return l;
    }
    
    public static Object lerPedido(long id){
        return ler(PDV_Pedido.class, id);
    }
}
