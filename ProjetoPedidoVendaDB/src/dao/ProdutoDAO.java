/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import entity.PRO_Produto;
import java.util.List;
import javax.persistence.Query;

/**
 *
 * @author GUSTAVO
 */
public class ProdutoDAO extends DAO {
    private static final long serialVersionUID = 1L;

    @SuppressWarnings("unchecked")
    public static List<PRO_Produto> listar() {
        Query q = criarQuery("SELECT p FROM PRO_Produto p ORDER BY p.PRO_ProdutoCodigo");
        List<PRO_Produto> l = q.getResultList();
        System.out.println("total: "+ l.size());
        return l;
    }
    
    public static Object ler(String Produto){
        return lerProduto(PRO_Produto.class, Produto);
    }
        
}
