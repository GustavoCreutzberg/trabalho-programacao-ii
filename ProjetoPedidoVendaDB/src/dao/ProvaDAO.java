/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import static dao.DAO.criarQuery;
import entity.Prova;
import java.util.List;
import javax.persistence.Query;

/**
 *
 * @author GUSTAVO
 */
public class ProvaDAO extends DAO{
    private static final long serialVersionUID = 1L;

    @SuppressWarnings("unchecked")
    public static List<Prova> listar() {
        Query q = criarQuery("SELECT p FROM Prova p ORDER BY p.id");
        List<Prova> l = q.getResultList();
        System.out.println("total: "+ l.size());
        return l;
    }
    
    public static Object ler(long id){
        return ler(Prova.class, id);
    }
}
