
package Model;

public class EMP_Pessoa {
    private String EMP_pessoaNome;
    private int EMP_pessoaCodigo;
    private String EMP_pessoaTipo;
    
    public EMP_Pessoa(){
        this.EMP_pessoaNome = "";
        this.EMP_pessoaCodigo = 0;
        this.EMP_pessoaTipo = "";
        
    }
    
    @Override
    public String toString() {
        return "PDV_Pessoa{" + "EMP_pessoaNome=" + EMP_pessoaNome + ", EMP_pessoaCodigo=" + EMP_pessoaCodigo + ", EMP_pessoaTipo=" + EMP_pessoaTipo + '}';
    }

    public EMP_Pessoa(String EMP_pessoaNome, int EMP_pessoaCodigo, String EMP_pessoaTipo) {
        this.EMP_pessoaNome = EMP_pessoaNome;
        this.EMP_pessoaCodigo = EMP_pessoaCodigo;
        this.EMP_pessoaTipo = EMP_pessoaTipo;
    }

    public String getEMP_pessoaNome() {
        return EMP_pessoaNome;
    }

    public void setEMP_pessoaNome(String EMP_pessoaNome) {
        this.EMP_pessoaNome = EMP_pessoaNome;
    }

    public int getEMP_pessoaCodigo() {
        return EMP_pessoaCodigo;
    }

    public void setEMP_pessoaCodigo(int EMP_pessoaCodigo) {
        this.EMP_pessoaCodigo = EMP_pessoaCodigo;
    }

    public String getEMP_pessoaTipo() {
        return EMP_pessoaTipo;
    }

    public void setEMP_pessoaTipo(String EMP_pessoaTipo) {
        this.EMP_pessoaTipo = EMP_pessoaTipo;
    }
}
