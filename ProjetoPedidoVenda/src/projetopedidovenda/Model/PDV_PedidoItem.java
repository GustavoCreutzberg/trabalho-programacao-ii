package projetopedidovenda.Model;

/**
 *
 * @author GUSTAVO
 */
public class PDV_PedidoItem {
    
    static PDV_PedidoItem PedidoItem[] = new PDV_PedidoItem[100];
    
    public int PDV_PedidoSeq;
    public int PDV_PedidoItemSeq;
    public String PDV_PedidoItemProdutoCodigo;
    public Double PDV_PedidoItemQuantidada;
    public Double PDV_PedidoItemValorUnitario;
    public Double PDV_PedidoItemValorTotal;

    public PDV_PedidoItem() {
        this.PDV_PedidoSeq = 0;
        this.PDV_PedidoItemSeq = 0;
        this.PDV_PedidoItemProdutoCodigo = "";
        this.PDV_PedidoItemQuantidada = 0.0;
        this.PDV_PedidoItemValorUnitario = 0.0;
        this.PDV_PedidoItemValorTotal = 0.0;
    }
    
    public PDV_PedidoItem(int PDV_PedidoSeq, int PDV_PedidoItemSeq, String PDV_PedidoItemProdutoCodigo, Double PDV_PedidoItemQuantidada, Double PDV_PedidoItemValorUnitario, Double PDV_PedidoItemValorTotal) {
        this.PDV_PedidoSeq = PDV_PedidoSeq;
        this.PDV_PedidoItemSeq = PDV_PedidoItemSeq;
        this.PDV_PedidoItemProdutoCodigo = PDV_PedidoItemProdutoCodigo;
        this.PDV_PedidoItemQuantidada = PDV_PedidoItemQuantidada;
        this.PDV_PedidoItemValorUnitario = PDV_PedidoItemValorUnitario;
        this.PDV_PedidoItemValorTotal = PDV_PedidoItemValorTotal;
    }

    public int getPDV_PedidoSeq() {
        return PDV_PedidoSeq;
    }

    public void setPDV_PedidoSeq(int PDV_PedidoSeq) {
        this.PDV_PedidoSeq = PDV_PedidoSeq;
    }

    public int getPDV_PedidoItemSeq() {
        return PDV_PedidoItemSeq;
    }

    public void setPDV_PedidoItemSeq(int PDV_PedidoItemSeq) {
        this.PDV_PedidoItemSeq = PDV_PedidoItemSeq;
    }

    public String getPDV_PedidoItemProdutoCodigo() {
        return PDV_PedidoItemProdutoCodigo;
    }

    public void setPDV_PedidoItemProdutoCodigo(String PDV_PedidoItemProdutoCodigo) {
        this.PDV_PedidoItemProdutoCodigo = PDV_PedidoItemProdutoCodigo;
    }

    public Double getPDV_PedidoItemQuantidada() {
        return PDV_PedidoItemQuantidada;
    }

    public void setPDV_PedidoItemQuantidada(Double PDV_PedidoItemQuantidada) {
        this.PDV_PedidoItemQuantidada = PDV_PedidoItemQuantidada;
    }

    public Double getPDV_PedidoItemValorUnitario() {
        return PDV_PedidoItemValorUnitario;
    }

    public void setPDV_PedidoItemValorUnitario(Double PDV_PedidoItemValorUnitario) {
        this.PDV_PedidoItemValorUnitario = PDV_PedidoItemValorUnitario;
    }

    public Double getPDV_PedidoItemValorTotal() {
        return PDV_PedidoItemValorTotal;
    }

    public void setPDV_PedidoItemValorTotal(Double PDV_PedidoItemValorTotal) {
        this.PDV_PedidoItemValorTotal = PDV_PedidoItemValorTotal;
    }

    @Override
    public String toString() {
        return "PDV_PedidoItem{" + "PDV_PedidoSeq=" + PDV_PedidoSeq + ", PDV_PedidoItemSeq=" + PDV_PedidoItemSeq + ", PDV_PedidoItemProdutoCodigo=" + PDV_PedidoItemProdutoCodigo + ", PDV_PedidoItemQuantidada=" + PDV_PedidoItemQuantidada + ", PDV_PedidoItemValorUnitario=" + PDV_PedidoItemValorUnitario + ", PDV_PedidoItemValorTotal=" + PDV_PedidoItemValorTotal + '}';
    }
    
    public void AddPedidoItem(PDV_PedidoItem item) {
        int Seq = 0;/*
        for (int i = 0; i < PedidoItem.length; i++) {
            if (PedidoItem[i].PDV_PedidoItemSeq >= 0){
                Seq = i++;
            }
        }
        PedidoItem[Seq].PDV_PedidoSeq = item.PDV_PedidoSeq;
        PedidoItem[Seq].PDV_PedidoItemSeq = item.PDV_PedidoItemSeq;
        PedidoItem[Seq].PDV_PedidoItemProdutoCodigo = item.PDV_PedidoItemProdutoCodigo;        
        PedidoItem[Seq].PDV_PedidoItemQuantidada = item.PDV_PedidoItemQuantidada;
        PedidoItem[Seq].PDV_PedidoItemValorUnitario = item.PDV_PedidoItemValorUnitario;
        PedidoItem[Seq].PDV_PedidoItemValorTotal = item.PDV_PedidoItemValorTotal;*/
    }
    
}
