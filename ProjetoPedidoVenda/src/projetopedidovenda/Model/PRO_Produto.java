
package Model;


public class PRO_Produto {
    private String PDV_produtoCodigo;
    private String PDV_produtoDescricao;
    private Double PDV_valorUnitario;
    
    public PRO_Produto (){
        this.PDV_produtoCodigo = "";
        this.PDV_produtoDescricao = "";
        this.PDV_valorUnitario = 0.0;
        
    }

    @Override
    public String toString() {
        return "PDV_Produto{" + "PDV_produtoCodigo=" + PDV_produtoCodigo + ", PDV_produtoDescricao=" + PDV_produtoDescricao + ", PDV_valorUnitario=" + PDV_valorUnitario + '}';
    }

    public PRO_Produto(String PDV_produtoCodigo, String PDV_produtoDescricao, Double PDV_valorUnitario) {
        this.PDV_produtoCodigo = PDV_produtoCodigo;
        this.PDV_produtoDescricao = PDV_produtoDescricao;
        this.PDV_valorUnitario = PDV_valorUnitario;
    }

    public String getPDV_produtoCodigo() {
        return PDV_produtoCodigo;
    }

    public void setPDV_produtoCodigo(String PDV_produtoCodigo) {
        this.PDV_produtoCodigo = PDV_produtoCodigo;
    }

    public String getPDV_produtoDescricao() {
        return PDV_produtoDescricao;
    }

    public void setPDV_produtoDescricao(String PDV_produtoDescricao) {
        this.PDV_produtoDescricao = PDV_produtoDescricao;
    }

    public Double getPDV_valorUnitario() {
        return PDV_valorUnitario;
    }

    public void setPDV_valorUnitario(Double PDV_valorUnitario) {
        this.PDV_valorUnitario = PDV_valorUnitario;
    }
    
}
