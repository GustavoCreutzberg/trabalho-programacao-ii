package projetopedidovenda.Model;

/**
 *
 * @author GUSTAVO
 */
public class PDV_Pedido {
   
    static PDV_Pedido Pedido[] = new PDV_Pedido[100];
    
    private int PDV_PedidoSeq;
    private String PDV_PedidoSituacao;
    private String PDV_PedidoPessoaCodigo;
    private String PDV_PedidoFormaPagto;
    private String PDV_PedidoCondPagto;
    private String PDV_PedidoDataEmissao;
    private Double PDV_PedidoValorTotal;	

    public PDV_Pedido() {
        this.PDV_PedidoSeq = 0;
        this.PDV_PedidoSituacao = "";
        this.PDV_PedidoPessoaCodigo = "";
        this.PDV_PedidoFormaPagto = "";
        this.PDV_PedidoCondPagto = "";
        this.PDV_PedidoDataEmissao = "";
        this.PDV_PedidoValorTotal = 0.0;
    }
    
    public PDV_Pedido(int PDV_PedidoSeq, String PDV_PedidoSituacao, String PDV_PedidoPessoaCodigo, String PDV_PedidoFormaPagto, String PDV_PedidoCondPagto, String PDV_PedidoDataEmissao, Double PDV_PedidoValorTotal) {
        this.PDV_PedidoSeq = PDV_PedidoSeq;
        this.PDV_PedidoSituacao = PDV_PedidoSituacao;
        this.PDV_PedidoPessoaCodigo = PDV_PedidoPessoaCodigo;
        this.PDV_PedidoFormaPagto = PDV_PedidoFormaPagto;
        this.PDV_PedidoCondPagto = PDV_PedidoCondPagto;
        this.PDV_PedidoDataEmissao = PDV_PedidoDataEmissao;
        this.PDV_PedidoValorTotal = PDV_PedidoValorTotal;
    }

    public int getPDV_PedidoSeq() {
        return PDV_PedidoSeq;
    }

    public void setPDV_PedidoSeq(int PDV_PedidoSeq) {
        this.PDV_PedidoSeq = PDV_PedidoSeq;
    }

    public String getPDV_PedidoSituacao() {
        return PDV_PedidoSituacao;
    }

    public void setPDV_PedidoSituacao(String PDV_PedidoSituacao) {
        this.PDV_PedidoSituacao = PDV_PedidoSituacao;
    }
    
    public String getPDV_PedidoPessoaCodigo() {
        return PDV_PedidoPessoaCodigo;
    }

    public void setPDV_PedidoPessoaCodigo(String PDV_PedidoPessoaCodigo) {
        this.PDV_PedidoPessoaCodigo = PDV_PedidoPessoaCodigo;
    }

    public String getPDV_PedidoFormaPagto() {
        return PDV_PedidoFormaPagto;
    }

    public void setPDV_PedidoFormaPagto(String PDV_PedidoFormaPagto) {
        this.PDV_PedidoFormaPagto = PDV_PedidoFormaPagto;
    }

    public String getPDV_PedidoCondPagto() {
        return PDV_PedidoCondPagto;
    }

    public void setPDV_PedidoCondPagto(String PDV_PedidoCondPagto) {
        this.PDV_PedidoCondPagto = PDV_PedidoCondPagto;
    }

    public String getPDV_PedidoDataEmissao() {
        return PDV_PedidoDataEmissao;
    }

    public void setPDV_PedidoDataEmissao(String PDV_PedidoDataEmissao) {
        this.PDV_PedidoDataEmissao = PDV_PedidoDataEmissao;
    }

    public Double getPDV_PedidoValorTotal() {
        return PDV_PedidoValorTotal;
    }

    public void setPDV_PedidoValorTotal(Double PDV_PedidoValorTotal) {
        this.PDV_PedidoValorTotal = PDV_PedidoValorTotal;
    }

    @Override
    public String toString() {
        return "PDV_Pedido{" + "PDV_PedidoSeq=" + PDV_PedidoSeq + ", PDV_PedidoSituacao=" + PDV_PedidoSituacao + ", PDV_PedidoPessoaCodigo=" + PDV_PedidoPessoaCodigo + ", PDV_PedidoFormaPagto=" + PDV_PedidoFormaPagto + ", PDV_PedidoCondPagto=" + PDV_PedidoCondPagto + ", PDV_PedidoDataEmissao=" + PDV_PedidoDataEmissao + ", PDV_PedidoValorTotal=" + PDV_PedidoValorTotal + '}';
    }

}
